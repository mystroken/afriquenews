<?php

namespace AppBundle\Controller;

use FastFeed\Aggregator\RSSContentAggregator;
use FastFeed\FastFeed;
use FastFeed\Parser\RSSParser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Guzzle\Http\Client;
use Monolog\Logger;

class DefaultController extends Controller
{
    /**
     * @Route("/ffeed", name="fast_feed")
     */
    public function fastFeedAction(Request $request)
    {
        // Instantiate Fast Feed: Factory way
        /**
         * @var $client \Guzzle\Http\Client
         */
        $client = new Client();
        /**
         * @var Logger
         */
        $logger = $this->get('logger');
        $fastFeed = new FastFeed($client, $logger);
        $parser = new RSSParser();
        $parser->pushAggregator(new RSSContentAggregator());
        $fastFeed->pushParser($parser);

        // Add feeds
        //$fastFeed->addFeed('default', 'https://www.afdb.org/fr/news-and-events/rss/');
        $fastFeed->addFeed('default', 'http://www.ecofinance.sn/xml/syndication.rss');

        // Enjoy
        $items = $fastFeed->fetch('default');
        foreach ($items as $item) {
            //echo '<h1>' . $item->getName() . '</h1>' . PHP_EOL;
            var_dump($item);
        }
    }

    /**
     * @Route("/feedio", name="feed_io")
     */
    public function feedIoAction(Request $request)
    {
        // get feedio
        $feedIo = $this->container->get('feedio');

        // this date is used to fetch only the latest items
        $modifiedSince = new \DateTime();

        // the feed you want to read
        //$url = 'http://www.un.org/africarenewal/rss.xml';
        $url = 'http://www.camer.be/rss.php';

        // now fetch its (fresh) content
        $feed = $feedIo->read($url)->getFeed();

        foreach ( $feed as $item ) {
            //echo "item title : {$item->getTitle()} \n ";
            // getMedias() returns enclosures if any
            $medias = $item->getMedias();
            var_dump($item);
        }
    }
}
