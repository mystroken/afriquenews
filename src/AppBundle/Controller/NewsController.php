<?php

namespace AppBundle\Controller;

use FastFeed\Factory;
use FastFeed\Parser\RSSParser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class NewsController
 * @package AppBundle\Controller
 */
class NewsController extends Controller
{
    /**
     * @Route("/", name="news_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository('AppBundle:Article')->findBy([], ['publishedAt' => 'DESC']);

        return $this->render('news/index.html.twig', array(
            'articles' => $articles
        ));
    }

}
