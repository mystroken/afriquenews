<?php

namespace AppBundle\Util;

use FastFeed\Item;
use FastFeed\Processor\ProcessorInterface;

/**
 * Class LimitByDateProcessor
 * @package AppBundle\Util
 */
class LimitByDateProcessor implements ProcessorInterface
{

    /**
     * @var \DateTime
     */
    protected $minimumDate = null;

    /**
     * LimitByDateProcessor constructor.
     * @param \DateTime $minimumDate
     */
    public function __construct(\DateTime $minimumDate)
    {
        $this->minimumDate = $minimumDate;
    }

    /**
     * Execute processor
     *
     * @param array $items
     *
     * @return array $items
     */
    public function process(array $items)
    {
        if(is_null($this->minimumDate))
        {
            return $items;
        }

        $total = count($items);
        $filteredItems = array();

        for ($i = 0; $i < $total; $i++)
        {
            /** @var Item $item */
            $item = $items[$i];

            if($item->getDate()->getTimestamp() > $this->minimumDate->getTimestamp())
            {
                $filteredItems[] = $item;
            }
        }
        return $filteredItems;
    }
}