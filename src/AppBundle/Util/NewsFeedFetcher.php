<?php

namespace AppBundle\Util;

use AppBundle\Entity\Article;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use FastFeed\FastFeed;
use FastFeed\Parser\RSSParser;
use Guzzle\Http\Client;
use Monolog\Logger;

/**
 * Class NewsFeedFetcher
 *
 * Fetch news feed from registered channels into database
 * and store fetched articles to the database
 *
 * @package AppBundle\Util
 */
class NewsFeedFetcher implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container, EntityManager $manager)
    {
        $this->container = $container;
        $this->em = $manager;
    }

    public function onTerminate()
    {
        // Instantiate Fast Feed with RSS Parser
        $client = new Client();
        $logger = $this->container->get('logger');
        $fastFeed = new FastFeed($client, $logger);
        $fastFeed->pushParser(new RSSParser());

        // Fetch feeds then add them to the database
        $channels = $this->em->getRepository('AppBundle:Channel')->findAll();
        foreach ($channels as $channel)
        {
            $fastFeed->addFeed($channel->getName(), $channel->getFeed());

            // Use our processor to filter only un-fetched items
            if( !is_null($channel->getFetchedAt()) )
            {
                $limitByDateProcessor = new LimitByDateProcessor($channel->getFetchedAt());
                $fastFeed->pushProcessor($limitByDateProcessor);
            }

            $items = $fastFeed->fetch($channel->getName());

            foreach ($items as $item)
            {
                $article = new Article;

                $article->setTitle($item->getName());
                $article->setContent($item->getContent());
                $article->setUrl($item->getSource());
                $article->setAuthor($item->getAuthor());
                $article->setPublishedAt($item->getDate());
                $article->setChannel($channel);

                $this->em->persist($article);
            }

            if( !is_null($channel->getFetchedAt()) ) $fastFeed->popProcessor();
            $channel->setFetchedAt(new \DateTime);
            $this->em->persist($channel);
        }

        $this->em->flush();
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::TERMINATE => 'onTerminate'
        );
    }
}