<?php

namespace AppBundle\Util;
use Doctrine\ORM\EntityManager;

/**
 * Class DatabaseFeeder
 *
 * Feeds the database with articles
 *
 * @package AppBundle\Util
 */
class DatabaseFeeder
{
    /** @var  EntityManager */
    protected $em;

    /**
     * DatabaseFeeder constructor.
     * @param EntityManager $manager
     */
    public function __construct(EntityManager $manager)
    {
    }
}