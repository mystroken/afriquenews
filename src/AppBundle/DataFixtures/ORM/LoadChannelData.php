<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Channel;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadChannelData implements FixtureInterface
{
    public function load(ObjectManager $em)
    {
        $camerbe = new Channel();
        $camerbe
            ->setName('camer.be')
            ->setLanguage('fr')
            ->setUrl('http://www.camer.be')
            ->setFeed('http://www.camer.be/rss.php')
        ;

        $africarenewal = new Channel();
        $africarenewal
            ->setName('africarenewal')
            ->setLanguage('en')
            ->setUrl('http://www.un.org/africarenewal')
            ->setFeed('http://www.un.org/africarenewal/rss.xml')
        ;

        $ecofinance = new Channel();
        $ecofinance
            ->setName('ecofinance')
            ->setLanguage('fr')
            ->setUrl('http://www.ecofinance.sn')
            ->setFeed('http://www.ecofinance.sn/xml/syndication.rss')
        ;

        $jeuneafrique = new Channel();
        $jeuneafrique
            ->setName('jeuneafrique news')
            ->setLanguage('fr')
            ->setUrl('http://www.jeuneafrique.com')
            ->setFeed('http://www.jeuneafrique.com/feed/')
        ;

        $em->persist($camerbe);
        $em->persist($africarenewal);
        $em->persist($ecofinance);
        $em->persist($jeuneafrique);

        $em->flush();
    }
}